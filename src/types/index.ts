export interface RecordFields {
  name_nl: string
  startdate: string
  enddate: string
  description_nl: string
  outdoors: string
  isaccessibleforfree: string
}

export interface Record {
  recordid: number
  fields: RecordFields
}

export interface APIResult {
  nhits: number
  parameters: {
    dataset: string
    q: string
    rows: number
    start: number
    sort: string[]
    format: string
    timezone: string
  }
  records: Record[]
}

export interface FilterValues {
  outdoors: boolean
  date: string
  free: boolean
}