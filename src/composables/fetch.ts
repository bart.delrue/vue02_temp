import type { MaybeRef } from 'vue'
import { isRef, ref, toValue, watchEffect } from 'vue'

export function useFetch<T>(url: MaybeRef<string>, requestInit: RequestInit = {}) {
  const result = ref<T | null>(null)
  const error = ref<Error | null>()
  const isLoading = ref<boolean>(false)

  return {
    result,
    error,
    isLoading
  }
}
