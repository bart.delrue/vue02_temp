import type { MaybeRef } from 'vue'
import { isRef, ref, toValue, watchEffect } from 'vue'

interface GhentTimeOptions {
  start: MaybeRef<string>
  end?: MaybeRef<string>
}

export function useGhentTime({ start, end }: GhentTimeOptions) {
  const ghentTimeString = ref<string>()

  const toDate = function (date: Date) {
    return date.toLocaleString('nl-BE', {
      weekday: 'short',
      day: 'numeric',
      month: 'short'
    })
  }
  const toHour = function (date: Date) {
    return date.toLocaleString('nl-BE', {
      hour: 'numeric',
      minute: 'numeric'
    })
  }

  const toGhentTime = function () {
    const localStart = new Date(toValue(start))
    if (!end) {
      ghentTimeString.value = toDate(localStart) + ' om ' + toHour(localStart)
      return
    }

    const localEnd = new Date(toValue(end))
    if (localEnd.getDate() === localStart.getDate()) {
      ghentTimeString.value =
        toDate(localStart) + ' van ' + toHour(localStart) + ' tot ' + toHour(localEnd)
      return
    }

    ghentTimeString.value =
      toDate(localStart) +
      ' om ' +
      toHour(localStart) +
      ' tot ' +
      toDate(localEnd) +
      ' om ' +
      toHour(localEnd)
  }

  return { ghentTimeString }
}
